# created by Philip Orlando @ Sustainable Atmopsheres Research Lab
# PI Dr. Linda George
# 2018-07-12
# Explore slopes over time
# Is there a difference in slope based on source?

# load the necessary packages
if (!require(pacman)) {
  install.packages("pacman")
  library(pacman)
}

# load necessary packages
p_load(readr
       ,ggplot2
       ,plyr
       ,dplyr
       ,broom
       ,reshape2
       ,tidyr
       ,stringr
       ,magrittr
       ,rlang
       ,qdapRegex
)


# creating a custon not-in function
'%!in%' <- function(x,y)!('%in%'(x,y))

# read in summary stats for PurpleAirCalibrations
df <- read.csv("./data/Output/PurpleAirSummaryTable.txt")

# examine structure of df
str(df)

# convert start/end times to POSIXct
df$start_time <- as.POSIXct(df$start_time)
df$end_time <- as.POSIXct(df$end_time)

# create pm_source variable for Candle or Match test
df$pm_source <- ifelse(df$start_time >= as.POSIXct("2018-06-29"), "Candle", "Match")

teensy <- c("Teensy_1"
            ,"Teensy_2"
            ,"Teensy_3"
            ,"Teensy_4"
            ,"Teensy_5"
            ,"Teensy_6"
            ,"Teensy_7"
            ,"Teensy_8"
            ,"Teensy_9"
            ,"Teensy_10")


# remove DustTrak data and exclude slopes that are outliers (not supposed to be included in pipeline... not cherrypicking)
df <- filter(df, sensor != "DustTrak404" & sensor %!in% teensy) %>%
  dplyr::filter(pollutant == "pm2_5_atm" & slope > 0 & slope < 10 )
write.csv(df, "./data/Output/PurpleAirSummary.txt")

df %>% filter(pm_source == "Match") %>% nrow()
df %>% filter(pm_source == "Candle") %>% nrow()

cor <- df %>%
  dplyr::select(sensor, r_squared, pm_source, slope) %>%
  dplyr::group_by(sensor, pm_source) %>%
  dplyr::summarise(r2 = mean(r_squared, na.rm = T), r2_sd = sd(r_squared, na.rm = T), m = mean(slope, na.rm = T), m_sd = sd(slope, na.rm = T))


cor_total <- df %>%
  dplyr::select(sensor, r_squared, pm_source, slope) %>%
  dplyr::group_by(pm_source) %>%
  dplyr::summarise(r2 = mean(r_squared, na.rm = T), r2_sd = sd(r_squared, na.rm = T), m = mean(slope, na.rm = T), m_sd = sd(slope, na.rm = T))

cor_total

match <- cor %>% filter(pm_source == "Match")
candle <- cor %>% filter(pm_source == "Candle")

match %>% filter(r2 > 0.90) %>% nrow() / nrow(match)
candle %>% filter(r2 > 0.90) %>% nrow() / nrow(candle)
cor %>% filter(r2 > 0.90) %>% nrow() / nrow(cor)

match %>% summary()
candle %>% summary()

# define global graphics variables ------------------------------------------------------------------------------
plot_width <- 4
plot_height <- 3
plot_dpi <- 300

plot_title_size <- 12
plot_subtitle_size <- 10
axis_text_size <- 0.8
axis_title_size <- 8
legend_title_size <- 10
legend_text_size <- 8
outlier_size <- 0.5
box_line_width <- 0.5

lower_limit <- 0
upper_limit <- 1

lower_slope_limit <- -1
upper_slope_limit <- 5
# begin plotting  ------------------------------------------------------------------------------


# plot r_squared against time, encoded by pm source
p1 <- ggplot(filter(df, pollutant == "pm2_5_atm"), aes(x = start_time, y = r_squared, color = pm_source)) +
  geom_point() +
  theme_bw() +
  xlab("Time") + 
  ylab("R2") +
  labs(title = "PurpleAir Correlation by PM Source") +
  scale_y_continuous(limits = c(lower_limit, upper_limit)) +
  guides(color=guide_legend("PM Source")) +  # add guide properties by aesthetic
  theme_bw() + 
  theme(aspect.ratio = 1, plot.title = element_text(hjust = 0.5, size = plot_title_size, face = "bold"),
        plot.subtitle = element_text(hjust = 0.5, size=plot_subtitle_size, face = "bold"),
        axis.text = element_text(size=rel(axis_text_size), face = "bold", colour = "black"),
        axis.title = element_text(size=axis_title_size, face = "bold"),
        legend.title = element_text(size = legend_title_size, face = "bold"),
        legend.text = element_text(size = legend_text_size, face = "bold"))

p1

b1 <- ggplot(filter(df, pollutant == "pm2_5_atm"), aes(x = pm_source, y = r_squared, fill = pm_source)) +
  geom_boxplot(outlier.size = outlier_size, lwd = box_line_width, fatten = box_line_width) +
  theme_bw() +
  xlab("PM Source") + 
  ylab(expression(R^2)) +
  # labs(title = "PurpleAir Correlation by PM Source") +
  # scale_y_continuous(limits = c(lower_limit, upper_limit)) +
  # guides(fill=guide_legend("PM Source")) +  # add guide properties by aesthetic
  guides(fill=FALSE)+
  theme_bw() + 
  theme(aspect.ratio = 1, plot.title = element_text(hjust = 0.5, size = plot_title_size, face = "bold"),
        plot.subtitle = element_text(hjust = 0.5, size=plot_subtitle_size, face = "bold"),
        axis.text = element_text(size=rel(axis_text_size), face = "bold", colour = "black"),
        axis.title = element_text(size=axis_title_size, face = "bold"),
        legend.title = element_text(size = legend_title_size, face = "bold"),
        legend.text = element_text(size = legend_text_size, face = "bold"))

b1

v1 <- ggplot(filter(df, pollutant == "pm2_5_atm"), aes(x = pm_source, y = r_squared, fill = pm_source)) +
  geom_violin() +
  theme_bw() +
  xlab("PM Source") + 
  ylab(expression(R^2)) +
  labs(title = "PurpleAir Correlation with DustTrak") +
  # scale_y_continuous(limits = c(lower_limit, upper_limit)) +
  # guides(fill=guide_legend("PM Source")) +  # add guide properties by aesthetic
  guides(fill=FALSE)+
  theme_bw() + 
  theme(aspect.ratio = 1, plot.title = element_text(hjust = 0.5, size = plot_title_size, face = "bold"),
        plot.subtitle = element_text(hjust = 0.5, size=plot_subtitle_size, face = "bold"),
        axis.text = element_text(size=rel(axis_text_size), face = "bold", colour = "black"),
        axis.title = element_text(size=axis_title_size, face = "bold"),
        legend.title = element_text(size = legend_title_size, face = "bold"),
        legend.text = element_text(size = legend_text_size, face = "bold"))

v1


# plot slope against time, encoded by pm source
p2 <- ggplot(filter(df, pollutant == "pm2_5_atm"), aes(x = start_time, y = slope, color = pm_source)) +
  geom_point() +
  theme_bw() +
  xlab("Time") + 
  ylab("Slope") +
  labs(title = "PurpleAir Calibration Slope with DustTrak") +
  scale_y_continuous(limits = c(lower_slope_limit, upper_slope_limit)) +
  guides(color=guide_legend("PM Source")) +  # add guide properties by aesthetic
  theme_bw() + 
  # labs(title = "PurpleAir Slope ~ PM Source") +
  theme(aspect.ratio = 1, plot.title = element_text(hjust = 0.5, size = plot_title_size, face = "bold"),
        plot.subtitle = element_text(hjust = 0.5, size=plot_subtitle_size, face = "bold"),
        axis.text = element_text(size=rel(axis_text_size), face = "bold", colour = "black"),
        axis.title = element_text(size=axis_title_size, face = "bold"),
        legend.title = element_text(size = legend_title_size, face = "bold"),
        legend.text = element_text(size = legend_text_size, face = "bold"))
p2


b2 <- ggplot(filter(df, pollutant == "pm2_5_atm"), aes(x = pm_source, y = slope, fill = pm_source)) +
  geom_boxplot(outlier.size = outlier_size, lwd = box_line_width, fatten = box_line_width) +
  theme_bw() +
  xlab("PM Source") + 
  ylab("Slope") +
  scale_y_continuous(limits = c(lower_slope_limit, upper_slope_limit)) +
  # labs(title = "PurpleAir Calibration Slope with DustTrak") +
  # guides(fill=guide_legend("PM Source")) +  # add guide properties by aesthetic
  guides(fill=FALSE)+
  theme_bw() + 
  theme(aspect.ratio = 1, plot.title = element_text(hjust = 0.5, size = plot_title_size, face = "bold"),
        plot.subtitle = element_text(hjust = 0.5, size=plot_subtitle_size, face = "bold"),
        axis.text = element_text(size=rel(axis_text_size), face = "bold", colour = "black"),
        axis.title = element_text(size=axis_title_size, face = "bold"),
        legend.title = element_text(size = legend_title_size, face = "bold"),
        legend.text = element_text(size = legend_text_size, face = "bold"))

b2 


v2 <- ggplot(filter(df, pollutant == "pm2_5_atm"), aes(x = pm_source, y = slope, fill = pm_source)) +
  geom_violin() +
  theme_bw() +
  xlab("PM Source") + 
  ylab("Slope") +
  labs(title = "PurpleAir Calibration Slope with DustTrak") +
  scale_y_continuous(limits = c(lower_slope_limit, upper_slope_limit)) +
  # guides(fill=guide_legend("PM Source")) +  # add guide properties by aesthetic
  guides(fill=FALSE)+
  theme_bw() + 
  theme(aspect.ratio = 1, plot.title = element_text(hjust = 0.5, size = plot_title_size, face = "bold"),
        plot.subtitle = element_text(hjust = 0.5, size=plot_subtitle_size, face = "bold"),
        axis.text = element_text(size=rel(axis_text_size), face = "bold", colour = "black"),
        axis.title = element_text(size=axis_title_size, face = "bold"),
        legend.title = element_text(size = legend_title_size, face = "bold"),
        legend.text = element_text(size = legend_text_size, face = "bold"))

v2 



# Write figures to file ------------------------------------------------------------------------------
try(ggsave(filename = "./figures/summary/r_squared_pm_source.jpg",
           plot = p1,
           scale = 1,
           width = plot_width,
           height = plot_height,
           units = "in",
           dpi = plot_dpi))




try(ggsave(filename = "./figures/summary/r_squared_boxplot_lab.jpg",
           plot = b1,
           scale = 1,
           width = plot_width,
           height = plot_height,
           units = "in",
           dpi = plot_dpi))



try(ggsave(filename = "./figures/summary/r_squared_violin_lab.jpg",
           plot = v1,
           scale = 1,
           width = plot_width,
           height = plot_height,
           units = "in",
           dpi = plot_dpi))


try(ggsave(filename = "./figures/summary/slope_pm_source.jpg",
           plot = p2,
           scale = 1,
           width = plot_width,
           height = plot_height,
           units = "in",
           dpi = plot_dpi))

try(ggsave(filename = "./figures/summary/slope_boxplot_lab.jpg",
           plot = b2,
           scale = 1,
           width = plot_width,
           height = plot_height,
           units = "in",
           dpi = plot_dpi))


try(ggsave(filename = "./figures/summary/slope_violin_lab.jpg",
           plot = v2,
           scale = 1,
           width = plot_width,
           height = plot_height,
           units = "in",
           dpi = plot_dpi))


# unique observations per source
obs <- df %>% 
  filter(pollutant == "pm2_5_atm") %>%
  group_by(pm_source, sensor) %>%
  na.omit() %>% 
  mutate(n = n()) %>%
  dplyr::select(sensor, pm_source, pollutant, n)

# determine which pm source provides better results on average
results <- df %>%
  filter(pollutant == "pm2_5_atm") %>%
  group_by(pm_source) %>%
  na.omit() %>%
  summarise_if(is.numeric, funs(mean, n()))

write.csv(results, file = "./data/Output/summary_pm_source.csv")

